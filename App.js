import React from "react";
import { SafeAreaView, StyleSheet, Text } from "react-native";
import SignInscreen from "./src/screens/SignInScreen";
import Conge from "./src/screens/CongeScreen/Conge";
import Teletravail from "./src/screens/TeletravailScreen/Teletravail";
import HomeScreen from "./src/screens/HomeScreen/Home";
import Home from "./src/screens/Home/Home1";
import Login from "./src/screens/LoginScreen/Login";
const App = () => {
  return (
    <SafeAreaView style={styles.root}>
      <SignInscreen />
      {/* <Conge></Conge> */}
      {/* <Teletravail></Teletravail> */}
      {/* <HomeScreen></HomeScreen> */}
      {/* <Home></Home>
      <Login></Login> */}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "#F9FBFC",
  },
});
export default App;
