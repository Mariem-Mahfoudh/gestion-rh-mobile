import React from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";
// import { View, Text } from "react-native";

const TeletravailInput = ({
  value,
  setValue,
  placeholder,
  secureTextEntry,
}) => {
  // const CostumInput = () => {
  return (
    <View style={styles.container}>
      <TextInput
        value={value}
        onChangeText={setValue}
        placeholder={placeholder}
        style={styles.input}
        secureTextEntry={secureTextEntry}
      ></TextInput>

      {/* <Text> Hellooo</Text> */}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: "100%",
    borderColor: "#e8e8e8",
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    marginVertical: 5,
  },
  //   logo: {
  //     width: "70%",
  //     maxWidth: 300,
  //     maxHeight: 200,
  //   },
  input: {},
});
export default CongeInput;
