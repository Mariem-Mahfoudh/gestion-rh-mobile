import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  useWindowDimensions,
} from "react-native";
import Logo from "../../../assets/images/foodom-logo.png";
import CostumInput from "../../components/CostumInput/CostumInput";
import CostumButton from "../../components/CostumButton/CostumButton";

const SignInscreen = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { height } = useWindowDimensions();
  const onSignInPressed = () => {
    console.warn("Sign in");
  };
  return (
    <View style={styles.root}>
      {/* <Text> Sign In Screen </Text> */}
      <Image
        source={Logo}
        style={[styles.logo, { height: height * 0.3 }]}
        resizeMode="contain"
      ></Image>
      <CostumInput
        placeholder="Enter votre mail"
        value={username}
        setValue={setUsername}
      ></CostumInput>

      <CostumInput
        placeholder="Enter votre Mot de passe"
        value={password}
        setValue={setPassword}
        secureTextEntry={true}
      ></CostumInput>
      <CostumButton text="Connexion" onPress={onSignInPressed}></CostumButton>
    </View>
  );
};
const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    padding: 20,
  },
  logo: {
    width: "70%",
    maxWidth: 300,
    maxHeight: 200,
  },
});
export default SignInscreen;
