import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  useWindowDimensions,
} from "react-native";
import Logo from "../../../assets/images/foodom-logo.png";
import CongeInput from "../../components/CostumInput/CongeInput";
import CongeButton from "../../components/CostumButton/CongeButton";
import DatePicker from "react-native-datepicker";
import DateTimePicker from "@react-native-community/datetimepicker";
const Conge = () => {
  const [date, setDate] = useState("09-10-2021");
  const [open, setOpen] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { height } = useWindowDimensions();
  const onSignInPressed = () => {
    console.warn("Demande envoyée ");
  };
  return (
    <View style={styles.root}>
      <Text style={styles.title}> Demande de congé </Text>
      <Text style={styles.text}> Date de début</Text>
      <DatePicker
        style={styles.date}
        modal
        open={open}
        date={date}
        // onConfirm={(date) => {
        //   setOpen(false);
        //   setDate(date);
        // }}
        // onCancel={() => {
        //   setOpen(false);
        // }}
      />
      <Text style={styles.text}> Date fin</Text>
      <DatePicker
        style={styles.date}
        modal
        open={open}
        date={date}
        // onConfirm={(date) => {
        //   setOpen(false);
        //   setDate(date);
        // }}
        // onCancel={() => {
        //   setOpen(false);
        // }}
      />
      <Text style={styles.text}>Motif</Text>
      <CongeInput
        // placeholder="Enter votre Mot de passe"
        value={password}
        setValue={setPassword}
        secureTextEntry={true}
      ></CongeInput>
      <Text style={styles.text}>Joindre un text</Text>
      <CongeInput
        // placeholder="Enter votre Mot de passe"
        value={password}
        setValue={setPassword}
        secureTextEntry={true}
      ></CongeInput>
      <CongeButton text="Ajouter" onPress={onSignInPressed}></CongeButton>
    </View>
    // <View style={styles.container}>
    //   <Text style={styles.text}>Birth Date :</Text>
    //   <DatePicker
    //     style={styles.datePickerStyle}
    //     date={date}
    //     mode="date"
    //     placeholder="select date"
    //     format="DD/MM/YYYY"
    //     minDate="01-01-1900"
    //     maxDate="01-01-2000"
    //     confirmBtnText="Confirm"
    //     cancelBtnText="Cancel"
    //     customStyles={{
    //       dateIcon: {
    //         position: "absolute",
    //         right: -5,
    //         top: 4,
    //         marginLeft: 0,
    //       },
    //       dateInput: {
    //         borderColor: "gray",
    //         alignItems: "flex-start",
    //         borderWidth: 0,
    //         borderBottomWidth: 1,
    //       },
    //       placeholderText: {
    //         fontSize: 17,
    //         color: "gray",
    //       },
    //       dateText: {
    //         fontSize: 17,
    //       },
    //     }}
    //     onDateChange={(date) => {
    //       setDate(date);
    //     }}
    //   />
    // </View>
  );
};
const styles = StyleSheet.create({
  root: {
    // alignItems: "center",
    padding: 20,
  },
  logo: {
    alignItems: "center",
    width: "70%",
    maxWidth: 300,
    maxHeight: 200,
  },
  text: {
    textAlign: "left",
    fontWeight: "bold",
    marginVertical: 5,
  },
  title: {
    textAlign: "center",
    fontWeight: "bold",
    marginVertical: 10,
    fontSize: 30,
  },
  date: {
    textAlign: "center",
    marginVertical: 20,
    fontSize: 1000,
  },
  // container: {
  //   flex: 1,
  //   padding: 10,
  //   justifyContent: "center",
  //   alignItems: "center",
  //   backgroundColor: "#A8E9CA",
  // },
  // title: {
  //   textAlign: "left",
  //   fontSize: 20,
  //   fontWeight: "bold",
  // },
  // datePickerStyle: {
  //   width: 230,
  // },
  // text: {
  //   textAlign: "left",
  //   width: 230,
  //   fontSize: 16,
  //   color: "#000",
  // },
});
export default Conge;
